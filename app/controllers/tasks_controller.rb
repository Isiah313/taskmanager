class TasksController < ApplicationController
  before_action :set_task, only: [:edit, :update, :destroy]

  respond_to :html, :js

  def index
    @user = current_user
    if @user.soft_user?
      @tasks = Task.where(soft_token: @user.soft_token)
    else
      @tasks = @user.tasks.all
    end
    respond_with(@tasks)
  end

  def new
    @task = Task.new
    respond_with(@task)
  end

  def edit
  end

  def create
    @user = current_user
    @task = Task.new(task_params)

    if @user.soft_user?
      @task.soft_token = @user.soft_token
    end

    if @task.save
      flash[:success] = "Your task was created successfully!"
      respond_with(@task)

    else
      flash[:warning] = "Your task could not be created!"
      render 'edit'
    end
  end

  def update
    @task.update(task_params)
    flash[:notice] = "Your task was updated successfully!"
    respond_with(@task)
  end

  def destroy
    @task.destroy
    flash[:warning] = "Your task was deleted successfully!"
    respond_with(@task)
  end

  def count 
    @tasks.count
  end

  def complete
    @task = Task.find(params[:id])
    flash[:alert] = "Congrats on completing another task!"
    @task.mark_complete!
  end

  private
    def set_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:name, :description, :completed, :user_id)
    end
end