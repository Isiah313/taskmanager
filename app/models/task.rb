class Task < ActiveRecord::Base
  belongs_to :user
  validates :name, presence: true
  validates :description, presence: true
  default_scope -> { order(updated_at: :desc) }

  scope :complete, -> { where(complete: true) }
  scope :incomplete, -> { where(complete: nil) }

  def count
  	count
  end

  def mark_complete!
  	self.update_attribute(:complete, true)
  end
end
