Rails.application.routes.draw do
  resources :tasks
  devise_for :users, controllers: { registrations: "users/registrations" }

  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end

  root 'tasks#index'
  
  get '/complete/:id', to: 'tasks#complete', as: 'complete'
end
